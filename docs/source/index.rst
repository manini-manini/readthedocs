.. Tutorial on Documentation using Sphinx documentation master file, created by
   sphinx-quickstart on Sun Dec 12 16:23:30 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


MyWorkforce Bot Integration
*************************************

This is a documentation for how to connect to a MyWorkforce Bot conversation

Overview 
================================
1. Authentication
2. Start Conversation
3. Sending activities to the bot
4. Receiving activities from bot

Details
================
1. Authentication:
******************

    We use JWT token as authentication mechanism to protect our resources
    To obtain a valid JWT token you have to issue a POST request to our authentication endpoint with valid credentials

    A. POST https://gateway.myworkforce.ai/api/authenticate
        - **Body**
            .. code-block:: json

               {
                  "username": "your user name",
                  "password": "your user password"
               }

        - **Response**
            .. code-block:: json

               {
                  "id_token": "your_jwt_token"
               }

2. Start Conversation:
**********************

    To start a conversation with a bot you have to issue a POST request to our bot conversation end point

    A. POST: https://gateway.myworkforce.ai/replyapi/api/agents/{your_bot_id}/conversations
        - **Headers**:
               Authorization: Bearer your_jwt_token

        - **Body**
            .. code-block:: json

               {
                  "clientDetails": {
                     "cid": "str",
                     "mobile": "str",
                     "age": 0,
                     "gender": "str",
                     "address": "str",
                     "dob": "date",
                     "imageUrl": "str",
                     "firstName": "str",
                     "lastName": "str",
                     "locale": "str",
                     "timezone": 0,
                     "contactEmail": "str"
                  }
               }


        - **Response**
            .. code-block:: json

               {
                  "conversationId": "str",
                  "token": "str",
                  "websocketUrl": "str",
                  "subscriptionUrl": "str",
                  "sendUrl": "str"
               }

    B. Response explanation :
         - **conversationId**: id for the started conversation and to be used in subsequent requests
         - **token**: credential for the started conversation and allow people who have it to send and receive activities to/from the started conversation
         - **websocketUrl**: our websocket endpoint
         - **subscriptionUrl**: stomp endpoint to subscribe to it to listen to bot activities
         - **sendUrl**: stomp endpoint to send activities to your bot

    For the websocket connection to be authenticated you have to provide the obtained token in start conversation request while you are connection to our web socket as the following:
    `websocketUrl?conversation_token=’token obtained from start conversation request’`_


3. Sending activities to the bot:
*********************************

    After you connected to our websocket you can send activities to the ‘sendUrl’ as json with the following format:
      .. code-block:: json

         {
            "message": {
               "text": "str"
            }
         }

4. Receiving activities from bot:
*********************************

   After you connected to our websocket you can receive bot activities by subscribing to ‘subscriptionUrl’ and you will receive a json formatted activity as the following:
      .. code-block:: json

            {
               "message": {
                  "text": "str"
               }
            }



